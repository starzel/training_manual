.. Mastering Plone documentation master file, created by
   sphinx-quickstart on Fri Jan 18 15:10:23 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mastering Plone's documentation!
===========================================

Please note, this document is not a public one.
A more formal documentation that we frequent quite often and less often extend is here http://developer.plone.org/index.html

This document is not better than the one above. The one above gets more frequent updates and gets reviewed by more people than this document.

While we want to teach you something, we also want to make it entertaining.

In some parts of the document, entertation might be a slight exageration or bad mouthing. We hope that during our presentation, it came quite clear that we hate nothing of it, but if you only have your eyes to digest the information, this might get lost, and we do not want to explain ourselves to people who might get offended by the words chosen.

If you did not read the document yet, don't worry, we will not be swearing in the document or use ugly words. In theory you could read this document to your kids, it contains no mature only words, thus it stayed immature. Or something like that.


Contents:

.. toctree::
    :maxdepth: 2

    chapter01
    chapter02
    chapter03
    chapter04
    chapter05
    chapter06
    chapter07
    chapter08
    chapter09
    chapter10
    chapter11
    chapter12
    chapter13
    chapter14
    chapter15
    chapter16
    chapter17
    chapter18



Indices and tables
==================

* :ref:`search`

