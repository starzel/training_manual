﻿
5. Extending Plone with Add-ons (80 min) (Patrick)
================================================

Zope is extensible and so is Plone.
If you want to install an Add-on, you are going to install an Egg. Eggs consist of python files together with other needed files like page templates and the like and a bit of Metadata, bundled to a single archive file.

Eggs are younger than Zope. Zope needed something like eggs before there were eggs, and the Zope developers wrote their own system. Old, outdated Plone systems contain a lot of code that is not bundled in an egg. Older code did not have metadata to register things, instead you needed a special setup method. We don't need this method but you might see it in other code. It is usually used to register Archetypes code. Archetypes is the old content type system. We use Dexterity.


Extension technologies
----------------------

Ok, how do you extend Plone? This depends on what type of extension you want to create.
You can create extensions with new types of objects to add to your Plone site. Usually these are content types. You can create an extension that changes or extends functionality. For example to change the way Plone displays search results, or to make pictures searchable by adding a converter from jpg to text.

skin_folders
^^^^^^^^^^^^
Do you remember Acquisition? Skin Folders extend the concepts of Acquistion. Your Plone site has a folder named ``portal_skins``. This folder has a number of sub folders. The ``portal_skins`` folder has a property that defines in which order Plone searches for attributes or objects in each sub folder.

The Plone logo is in a skin folder.

By default, your site has a custom folder, and items are first searched for in that folder.

To customize the logo, you copy it into the custom folder, and change it there. This way you can change templates, CSS styles, images and behavior, because a container may contain python scripts.

GenericSetup
^^^^^^^^^^^^^
The next thing is *GenericSetup*. As the name clearly implies, *GenericSetup* is part of CMF.

GenericSetup is tough to master, I am afraid.

*GenericSetup* lets you define persistent configuration in XML files. *GenericSetup* parses the XML files and updates the persistent configuration according to the configuration. This is a step you have to run on your own!

You will see many objects in Zope or the ZMI that you can customize through the web. If they are well behaving, they can export their configuration via *GenericSetup* and import it again.

Typically you use *GenericSetup* to change workflows or add new content type definitions.

Components
^^^^^^^^^^
The last way is via *Components*.

A bit of history is in order.
When Zope started, object-oriented Design was **the** silver bullet.
Zope objects have more than 10 base classes.
After a while, XML and Components became the next silver bullet (Does anybody remember J2EE?).
The Zope developers decided that these new silver bullets look much, much cooler in their colts so they decided rewrite Zope with this technology.
As the new concepts were radically different from the old Zope concepts, the Zope developers renamed the new project to Zope 3. But it did not gain traction, the community somehow renamed it to Bluebream and this died off.

The component architecture itself is quite successful and the Zope developer extracted it into the Zope Toolkit. The Zope toolkit is part of Zope, and Plone developers use it extensively.


This is what you want to use.


What are components, what is ZCML
---------------------------------
What is the absolute simplest way to extend functionality?
Monkey Patching. In code, during load time I import some code and replace it with my code.

If I would want to have an extensible registry of icons for different content types, I could create a global dictionary, and whoever implements a new icon for a different content type, would add an entry to my dictionary during import time.

This does not scale. Multiple plugins might overwrite each other, you would explain people that they have to reorder the imports, and then, suddenly, you will to import feature A before B, B before C and C before A, or else you application won't work.

Here comes the Zope Component Architecture and ZCML to your rescue.
With ZCML you declare utilities, adapters and browser views in ZCML, which is a XML dialect.
During startup, Zope reads all these ZCML statements, validates that there are not two declarations trying to register the same components and only then registers everything.

This is a good thing. ZCML is by the way only *one* way to declare your configuration.
Grok pvides another way, where some python magic allows you to decorate your code directly with a decorater to make it an adapter. You can use both ZCML and grok together.
We will mostly use Grok in later code examples.
Please be aware that not everybody loves Grok. Some parts of the Plone community think that there may only be one configuration language, others are against adding the relative big dependency of Grok to Plone. One real problem is the fact that you cannot customize components declared with grok with jbot. This is probably fixable, though. In any case, if you start to write an extension that is reusable, convert your grok declarations to ZCML to get maximum acceptance.

Many people hate ZCML and avoid Zope because of it being XML.
Personally, I just find it cumbersome but even for me as a developer it offers a nice advantage.
Thanks to ZCML, I hardly ever have a hard time to find out what and where extensions or customizations. For me, ZCML files are like a phone book.

Installation
------------
Installation is a two-step process.
First, we must make our code available to Zope.
This means, that Zope can import the code, Buildout is responsible this.

*ssh* to your vagrant, and change the buildout.cfg files in training/zinstance.

There is a variable named eggs, which has multiple *eggs* as a value. Add the following eggs:

    * PloneFormGen
    * Products.LinguaPlone
    * Products.PloneTrueGallery
    * collective.plonetruegallery

Usually, one enters the eggs by adding one more line per egg into the configuration.
You must write the egg name indented, this way Buildout understands that the current line is part of the last variable and not a new variable.

.. sourcecode:: bash

    $ bin/buildout
    $ bin/instance fg


Now the code is importable from within Plone and everything got registered via ZCML.
But Plone is not configured to use this.
For this, you have to install the Extension in your Plone Site.

In your browser, go the Plone control panel, and open the Products Panel. You will see that you can install all 4 packages there.

Install them now.

This is what happens now: The GenericSetup profile of the product gets loaded. This does things like configuring new actions, registering new
content types or creating some content/configuration objects in your Plone site.

PloneFormGen (Philip)
---------------------

There a various methods to create forms in Plone:

* pure html in a view
* z3c.form, formlib or in Python deform prgrammatically
* PloneFormGen

Mit PFG kann man professionelle Formulare zusammenklicken. Wenn man bedenkt was die Alternatven sind wird klar wie cool PFG ist. Der angeblich komfortablen Formulargenerator in Typo3 ist ja schon schlimm. In Plone könnte man Formulare auch von Hand in html schreiben und in Python auslesen was oft auch eine einfache Methode ist. Wenn es komplexer sein soll dann eben z3c.forms. Aber dazu muss man ja immer programmieren. Wir machen das jetzt mal nicht sondern klicken uns ein Anmeldeformular für die Plone-Konferenz zusammen.

http://konferenz.plone.de/anmeldung

In fact the guys at fourdigts embedd the form in a iframe. Let's pretend otherwise.

* easy form to subscribe a newsletter?
* registration-form (Name, Food, Shirt-size etc.)
* Mail-Adapter
* DataSave Adapter


Internationalisation with LinguaPlone (Philip)
----------------------------------------------

* /plone_control_panel
* install installieren
* add german as language einstellen

   * /@@language-controlpanel -> Deutsch und Englisch auswählen
   * ZMI -> portal_languages -> "Display flags for language selection" aktivieren

* @@language-setup-folders -> Ordnerstruktur anlegen
* Englische Startseite anlegen
* Infos zum übersetzen (folder übersetzen, language_independent)

   http://plone.org/products/linguaplone/issues/250
   http://localhost:8080/Plone/@@language-setup-folders
   Seit Plone4 ist der Standardweg von Übersetzungen, das jede Sprache
   einen eigenen Folder bekommt. Wenn Inhalte übersetzt werden, wird

* die Datei automatisch in den richtigen Ordner kopiert.


Add 'bling' with PloneTrueGallery (10min) (Patrick)
---------------------------------------------------
I LOVE THE
PloneTrueGallery.
It is a role model on how to write a Plone Extension.
Instead of creating custom content types for Galleries, it integrates
with the Plone functionality to choose different views for folderish content types.
Lets try it!...


Customizing the design with plone.app.themeeditor (20min) (Philip)
------------------------------------------------------------------

* Installation
* explain UI
* change Logo (dowmload http://www.ploneconf.org/++theme++ploneconf.theme/images/logo.png)
* change Footer (colophon): add copyright (Phone: +31 26 44 22 700
  mailto:info@ploneconf.org)
* change some css::

    #visual-portal-wrapper {
        margin: 0 auto;
        position: relative;
        width: 980px;
    }


export customizations
---------------------

* export the customizations as an egg (ploneconf.customisations)


inspect the egg
---------------

* what is where?
* jbot, static etc.


Wir können nun das Design unserer Webseite anpassen. Wir können Erweiterungen installieren und einfache Aktionen einrichten. Aber:

* Können wir auf unserer neuen Webseite Talks einreichen?
* Können wir in einer Liste die wichtigsten Eigenschaften jedes Talks anzeigen?
* Können wir Besucher den Talk bewerten lassen?

Wir müssen oft strukturierte Daten speichern oder anzeigen können, bis zu einem gewissen Grad auch noch TTW, aber irgendwann erreichen wir eine Grenze. Wir werden im zweiten Teil zeigen, wie man neue Contenttypen anlegt und wie man neue Funktionalitäten schreibt.


5.1 Theming
===========

* Diazo
* Downloading and activating a theme
* Creating a new theme
* Diazo Theme editor
* Rules
* Old-school Themeing
* Deliverance
