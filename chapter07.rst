
7. Plone-IDE's (15min) (Patrick)
================================

* Show your editor
* Sublime
* Aptana
* vim
* emacs
* Our development-setup today


No development environment is complete without a good editor.
Everybody must decide on his own, what editor to use, Therefor, I won't walk through a single editor here.
My biggest productivity tool is not the editor.
It is the capability of performing a full text search through the complete Plone code. Either you use your editor for this, or the console and grep. And of course you need the right code to look for, we set this up earlier with the omelette.
Thanks to omelette, my SSD and plenty of ram, I can search through the code in less than a minute.
By looking into the code, I see how things are supposed to be used and many other things.
